string <- "Hello world!"
print (string)

# Data Frame
data_frame_function <- function () {
	# Create vector objects.
	city <- c("Tampa", "Seattle", "Hartford", "Denver")
	state <- c("FL", "WA", "CT", "CO")
	zipcode <- c(33602, 98104, 06161, 80294)

	# Combine above threee vectors into one data frame.
	address <- cbind(city, state, zipcode)

	# Print a header.
	cat("# # # # The First data frame\n")
	# print the data frame
	print(address)


	# Create another data frame with similar columns
	new.address <- data.frame(
		city = c("Lowry", "Charlotte"),
		state = c("CO", "FL"),
		zipcode = c("80230", "33949"),
		stringsAsFactors = FALSE
	)
	# Print a header.
	cat("# # # # The Second data frame\n")

	# print the data frame
	print(new.address)

	# Combine rows form both the data frames
	all.addresses <- rbind(address, new.address)

	# Print a header.
	cat("# # # The combined data frame
	") 

	# Print the result.
	print(all.addresses)

	# 导入MASS package
	library(MASS)
	merged.Pima <- merge(x = Pima.te, y = Pima.tr,
	   by.x = c("bp", "bmi"),
	   by.y = c("bp", "bmi")
	)
	print(merged.Pima)
	nrow(merged.Pima)

	print(ships)

	# 导入reshape package
	library("reshape")
	molten.ships <- melt(ships, id = c("type","year"))
	print(molten.ships)

	recasted.ship <- cast(molten.ships, type+year~variable,sum)
	print(recasted.ship)



}
data_frame_function()


operation_function <- function () {
	# 两个向量相加
	v <- c(2, 3, 4)
	t <- c(3, 3, 5)
	print (v + t)

	# 两个向量相减
	v <- c( 2,5.5,6)
	t <- c(8, 3, 4)
	print(v - t)

	# 两个向量相乘
	print(v*t)

	# 将第一个向量与第二个向量相除
	print(v/t)

	# 两个向量求余
	print(v%%t)

	# 两个向量相除求商
	print(v%/%t)

	# 将第二向量作为第一向量的指数
	print (v^t)

	# 关系运算符

	v <- c(2,5.5,6,9)
	t <- c(8,2.5,14,9)

	# 检查第一向量的每个元素是否大于第二向量的相应元素。
	print(v>t)

	# 检查第一个向量的每个元素是否小于第二个向量的相应元素
	print (v < t)

	# 检查第一个向量的每个元素是否等于第二个向量的相应元素
	print (v == t)

	# 检查第一向量的每个元素是否小于或等于第二向量的相应元素
	print (v <= t)

	# 检查第一向量的每个元素是否大于或等于第二向量的相应元素
	print (v >= t)

	# 检查第一个向量的每个元素是否不等于第二个向量的相应元素。
	print (v != t)

	# 逻辑运算符
	# 它被称为元素逻辑AND运算符。 它将第一向量的每个元素与第二向量的相应元素组合，并且如果两个元素都为TRUE，则给出输出TRUE。
	v <- c(3,1,TRUE,2+3i)
	t <- c(4,1,FALSE,2+3i)
	print(v&t)

	# 它被称为元素逻辑或运算符。 它将第一向量的每个元素与第二向量的相应元素组合，并且如果元素为真，则给出输出TRUE。
	v <- c(3,0,TRUE,2+2i)
	t <- c(4,0,FALSE,2+3i)
	print(v|t)

	# 它被称为逻辑非运算符。 取得向量的每个元素，并给出相反的逻辑值。
	print(!v)

	# 称为逻辑AND运算符。 取两个向量的第一个元素，并且只有两个都为TRUE时才给出TRUE。
	v <- c(3,0,TRUE,2+2i)
	t <- c(1,3,TRUE,2+3i)
	print(v&&t)

	# 称为逻辑OR运算符。 取两个向量的第一个元素，如果其中一个为TRUE，则给出TRUE。
	v <- c(0,0,TRUE,2+2i)
	t <- c(0,3,TRUE,2+3i)
	print(v||t)

	# 赋值运算
	cat("\n称为左分配 <-, =, <<-\n")
	# 称为左分配 <-, =, <<-
	v1 <- c(3,1,TRUE,2+3i)
	v2 <<- c(3,1,TRUE,2+3i)
	v3 = c(3,1,TRUE,2+3i)
	print(v1)
	print(v2)
	print(v3)

	# 换行
	cat("\nright assignment\n")
	# 右分配
	c(3,1,TRUE,2+3i) -> v1
	c(3,1,TRUE,2+3i) ->> v2 
	print(v1)
	print(v2)

	# 冒号运算符。 它为向量按顺序创建一系列数字。
	v = 2:8
	print(v)
	v <- 2:8
	print (v)
	v <<- 2:8
	print (v)

	# %in% :此运算符用于标识元素是否属于向量。
	v1 = 8
	v2 = 12
	t <- 1:10
	print(v1 %in% t)
	print(v2 %in% t)

	# %*%:此运算符用于将矩阵与其转置相乘。
	M = matrix(c(2, 6, 5, 1, 10, 4), nrow = 2, ncol = 3, byrow = TRUE)
	t = M %*% t(M)
	print (t)

	x <- 30L
	if (is.integer(x)) {
		print("X is an Integer")
	}

	x <- c("what", "is", "truth")
	if ("Truth" %in% x) {
		print("Truth is found")
	} else if("truth" %in% x) {
		print("truth is found")
	} else {
		print("No truth found")
	}

	x <- switch(
		3,
		"first",
		"second",
		"third",
		"fourth"
	)
	print(x)

}


# 变量
# 变量的数据类型
var_function <- function () {
	# 可以在程序中使用同一个变量时，一次又一次地更改变量的数据类型
	var_x <- "hello"
	cat ("The class of var_x is ", class(var_x), "\n")

	var_x <- 23.5
	cat ("The class of var_x is ", class(var_x), "\n")

	var_x <- 27L
	cat ("The class of var_x is ", class(var_x), "\n")

	#  变量赋值
	#  Assignment using equal operator
	var.1 = c(1, 2, 3)

	# Assignment using leftward operator
	var.2 <- c("learn", "R")

	# Assignment using rightward operator
	c(TRUE, 1) -> var.3

	print (var.1)
	cat ("var.1 is", var.1, "
		")
	cat ("var.2 is", var.2, "
		")
	cat ("var.3 is", var.3, " 
		")

	# 数据类型

	# Create the data frame
	BMI <- data.frame(
		gender = c("Male", "Male", "Female"),
		height = c(152, 171.5, 165),
		weight = c(81, 93, 78), 
		Age = c(42, 38, 26)
	)
	print(BMI)

	# Create a vector
	apple_colors <- c('green', 'green', 'yello', 'red', 'red', 'red', 'green')
	# create a factor object
	factor_apple <- factor(apple_colors)
	# print the factor
	print(factor_apple)
	print(nlevels(factor_apple))

	# Create an array
	array1 <- array(c('green', 'yellow'), dim = c(3, 3, 2))
	print(array1)

	# Create a matrix
	M = matrix(c('a', 'a', 'b','c', 'b', 'a'), nrow = 2, ncol = 3, byrow = TRUE)
	print(M)

	# create a list
	list1 <- list(c(2, 5, 3), 21.3, sin)
	print(list1)

	# Create a vector
	apple <- c('red', 'green', 'yellow')
	print(apple)
	# Get the class of the vector
	print(class(apple))



	# Logical（逻辑型，TRUE、FALSE）
	lv <- TRUE
	print(class(lv))

	# Numberic (数字：12.3，5，999)
	nv <- 23.5
	print(class(nv))

	# Integer（整型）	2L，34L，0L
	iv <- 1L
	print(class(iv))

	# Complex（复合型）	3 + 2i
	cv <- 2 + 5i
	print(class(cv))

	# Character（字符）	'a' , '"good", "TRUE", '23.4'
	charV <- "True"
	print(class(charV))

	# Raw（原型）"Hello" 被存储为 48 65 6c 6c 6f
	rv <- charToRaw("Hello")
	print (class(rv))
	print(rv)

	# 查找变量
	print("look up variable")
	print(ls())

	# List the variables starting with the pattern "var"
	print(ls(pattern = "var"))

	# 以点(.)开头的变量被隐藏，它们可以使用ls()函数的“all.names = TRUE”参数列出。
	print(ls(all.name = TRUE))

	# 删除变量var.3
	rm(var.3)
	# print(var.3)

}





